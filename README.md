# OpenML dataset: Used-Cars-Dataset

https://www.openml.org/d/43306

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Craigslist is the world's largest collection of used vehicles for sale, yet it's very difficult to collect all of them in the same place. I built a scraper for a school project and expanded upon it later to create this dataset which includes every used vehicle entry within the United States on Craigslist.

Content
This data is scraped every few months, it contains most all relevant information that Craigslist provides on car sales including columns like price, condition, manufacturer, latitude/longitude, and 18 other categories. For ML projects, consider feature engineering on location columns such as long/lat. For previous listings, check older versions of the dataset.

See https://github.com/AustinReese/UsedVehicleSearch

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43306) of an [OpenML dataset](https://www.openml.org/d/43306). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43306/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43306/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43306/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

